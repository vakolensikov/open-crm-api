export class CreateTokenDto {
  userId: string;
  refreshToken: string;
}
