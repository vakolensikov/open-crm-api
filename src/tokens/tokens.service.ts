import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Token, TokenDocument } from './schemas/token.schema';
import { Model } from 'mongoose';
import { CreateTokenDto } from './dto/create-token.dto';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';
const MAX_TOKEN_AGE = 7 * 24 * 60 * 60 * 1000;

@Injectable()
export class TokensService {
  constructor(
    @InjectModel(Token.name) private tokenModel: Model<TokenDocument>,
    private jwtService: JwtService,
    private usersService: UsersService,
  ) {}

  async save(dto: CreateTokenDto) {
    const { userId, refreshToken } = dto;
    // console.log(dto);
    const tokenData = await this.tokenModel.findOne({ userId });
    if (tokenData) {
      tokenData.refreshToken = refreshToken;

      return await tokenData.save();
    }
    return await this.tokenModel.create(dto);
  }

  generateTokens(payload): { accessToken: string; refreshToken: string } {
    return {
      accessToken: this.jwtService.sign(payload, {
        expiresIn: process.env.ACCESS_EXPIRES_IN,
        secret: process.env.ACCESS_SECRET_KEY,
      }),
      refreshToken: this.jwtService.sign(payload, {
        expiresIn: process.env.REFRESH_EXPIRES_IN,
        secret: process.env.REFRESH_SECRET_KEY,
      }),
    };
  }

  validateToken(token) {
    try {
      const data = this.jwtService.verify(token, {
        secret: process.env.REFRESH_SECRET_KEY,
      });
      return data;
    } catch (e) {
      console.log(e);
      return null;
    }
  }

  async refresh({ refreshToken }, response) {
    if (!refreshToken) {
      throw new UnauthorizedException();
    }
    const userData = this.validateToken(refreshToken);
    const tokenFromDb = await this.tokenModel.findOne({ refreshToken });

    if (!userData || !tokenFromDb) {
      throw new UnauthorizedException();
    }

    const { password, ...user } = await this.usersService.findById(
      userData._id,
    );

    const tokens = this.generateTokens(user);
    response.cookie('refreshToken', tokens.refreshToken, {
      httpOnly: true,
      maxAge: MAX_TOKEN_AGE,
    });
    await this.save({ userId: user._id, refreshToken: tokens.refreshToken });
    return { ...tokens, user };
  }
}
