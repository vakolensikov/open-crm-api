import { Controller, Get, Request, Res } from '@nestjs/common';
import { TokensService } from './tokens.service';

@Controller('tokens')
export class TokensController {
  constructor(private readonly tokensService: TokensService) {}
  @Get('refresh')
  refresh(@Request() req, @Res({ passthrough: true }) res) {
    return this.tokensService.refresh(req.cookies, res);
  }
}
