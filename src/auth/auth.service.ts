import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { CreateUserDto } from '../users/dto/create-user.dto';
import * as bcrypt from 'bcryptjs';
import { TokensService } from '../tokens/tokens.service';

const MAX_TOKEN_AGE = 7 * 24 * 60 * 60 * 1000;

@Injectable()
export class AuthService {
  constructor(
    private userService: UsersService,
    private tokenService: TokensService,
  ) {}

  async login({ password, ...user }, response) {
    const { accessToken, refreshToken } =
      this.tokenService.generateTokens(user);
    response.cookie('refreshToken', refreshToken, {
      httpOnly: true,
      maxAge: MAX_TOKEN_AGE,
    });
    await this.tokenService.save({ userId: user._id, refreshToken });
    return { accessToken, refreshToken, user };
  }

  async registration(dto: CreateUserDto) {
    const candidate = await this.userService.findByEmail(dto.email);

    if (candidate) {
      throw new HttpException('user already exist', HttpStatus.BAD_REQUEST);
    }

    const hashPassword = await bcrypt.hash(dto.password, 5);

    const { password, ...user } = await this.userService.create({
      ...dto,
      password: hashPassword,
    });
    const tokens = this.tokenService.generateTokens(user);
    return { ...tokens, user };
  }

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.userService.findOne(username);

    const passwordEquals = await bcrypt.compare(pass, user.password);

    if (user && passwordEquals) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }
}
