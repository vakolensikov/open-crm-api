import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { TasksModule } from './tasks/tasks.module';
import { TokensModule } from './tokens/tokens.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env'],
      isGlobal: true,
    }),
    MongooseModule.forRoot(
      `mongodb+srv://root:root@cluster0.9bitq.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`,
    ),
    UsersModule,
    AuthModule,
    TasksModule,
    TokensModule,
  ],
})
export class AppModule {}
