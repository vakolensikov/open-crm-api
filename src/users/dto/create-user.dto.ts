import { ApiProperty } from '@nestjs/swagger';
import { Role } from '../../roles/role.enum';

export class CreateUserDto {
  @ApiProperty({ example: 'admin', description: 'Login of user' })
  username: string;

  email: string;
  password: string;
  roles: Role[];
}
