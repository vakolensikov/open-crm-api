import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from './schemas/user.schema';
import { Model } from 'mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { Task, TaskDocument } from '../tasks/schemas/task.schema';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    @InjectModel(Task.name) private taskModel: Model<TaskDocument>,
  ) {}

  async create(dto: CreateUserDto) {
    try {
      const user = await this.userModel.create(dto);

      return user;
    } catch (e) {
      console.log(e);
      throw new HttpException('server error', HttpStatus.BAD_REQUEST);
    }
  }

  async getAll() {
    const users = await this.userModel.find(
      {},
      { username: 1, email: 1, roles: 1, tasks: 1 },
    );

    return users;
  }

  async getById(id) {
    const user = await this.userModel.findById(id).populate('tasks');

    return user;
  }

  async findById(id) {
    const user = await this.userModel.findById(id).lean();

    return user;
  }

  async findByEmail(email: string) {
    const candidate = await this.userModel.findOne({ email });

    return candidate;
  }

  async findOne(username: string): Promise<User | undefined> {
    const user = await this.userModel.findOne({ username }).lean();

    return user;
  }

  async delete(id: string) {
    // await this.userModel.deleteOne({ id });

    return id;
  }
}
