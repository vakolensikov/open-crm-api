import { Injectable } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Task, TaskDocument } from './schemas/task.schema';
import { Model } from 'mongoose';
import { User, UserDocument } from '../users/schemas/user.schema';

@Injectable()
export class TasksService {
  constructor(
    @InjectModel(Task.name) private taskModel: Model<TaskDocument>,
    @InjectModel(User.name) private userModel: Model<UserDocument>,
  ) {}

  async create(dto: CreateTaskDto) {
    const { user: userId } = dto;
    const task = await this.taskModel.create(dto);

    if (userId) {
      const user = await this.userModel.findById(userId);
      user.tasks.push(task._id);
      user.save();
    }

    return task;
  }

  async findAll() {
    const tasks = await this.taskModel.find();
    return tasks;
  }

  async findOne(id: string) {
    const task = await this.taskModel.findById(id).populate('user');

    return task;
  }

  update(id: number, updateTaskDto: UpdateTaskDto) {
    return `This action updates a #${id} task`;
  }

  async remove(id: string) {
    await this.taskModel.deleteOne({ id });

    return id;
  }
}
