import { ObjectId } from 'mongoose';

export class CreateTaskDto {
  title: string;
  description: string;
  user: ObjectId;
}
